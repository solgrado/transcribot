/****************************************************************************
**
** Copyright (C) 2021 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Transcribot.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef TECLA_H
#define TECLA_H

#include <QObject>
#include <QVector>
#include <QMap>


const int PULSACION_SIMPLE = 0;
const int PULSACION_CON_MAYUSCULA = 1;
const int PULSACION_CON_TILDE = 2;
const int PULSACION_CON_MAYUSCULA_Y_TILDE = 3;
const int BLOQ_MAYUSCULAS = 256;
const int TILDE = 257;
const int GRADO = 176;
const int EXCLAMACION = 161;
const int B = 98;

class Tecla : public QObject
{
    Q_OBJECT
public:
    explicit Tecla(QObject *parent = nullptr);
    void getTipoPulsacion(QString caracter, int &tecla, int &tipoPulsacion);

signals:
};



#endif // TECLA_H

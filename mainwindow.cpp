﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPlainTextEdit>
#include <QTextStream>
#include <QStatusBar>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QtGlobal>

QByteArray contenidoCalibData = "MODULE CalibData\n"
                                "\tPERS tooldata MyNewTool:=[TRUE,[[0,0,120],[1,0,0,0]],[1,[0,0,1],[1,0,0,0],0,0,0]];\n"
                                "\tTASK PERS wobjdata WO_Teclado:=[FALSE,TRUE,\"\",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];\n"
                                "\tTASK PERS wobjdata WO_Elevacion:=[FALSE,TRUE,\"\",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];\n"
                                "\tTASK PERS wobjdata WO_Pulsacion:=[FALSE,TRUE,\"\",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];\n"
                                "ENDMODULE";

QByteArray contenidoPGF = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n"
                          "<Program>\n"
                          "\t<Module>Module1.mod</Module>\n"
                          "\t<Module>CalibData.mod</Module>\n"
                          "</Program>";


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->toolBar->setIconSize(QSize(32,32));

    trayectoria = new Trayectoria;
    qbaEntrada = new QByteArray;
    qbaSalida.clear();

    connect(ui->comboBoxVelPulsar, SIGNAL(currentTextChanged(QString)), trayectoria, SLOT(setVelPulsar(QString)));
    connect(ui->comboBoxVelSoltar, SIGNAL(currentTextChanged(QString)), trayectoria, SLOT(setVelSoltar(QString)));
    connect(ui->comboBoxPrecPulsar, SIGNAL(currentTextChanged(QString)), trayectoria, SLOT(setPrecPulsar(QString)));
    connect(ui->comboBoxPrecSoltar, SIGNAL(currentTextChanged(QString)), trayectoria, SLOT(setPrecSoltar(QString)));
    connect(ui->comboBoxVelDespLibre, SIGNAL(currentTextChanged(QString)), trayectoria, SLOT(setVelDespLibre(QString)));

#ifdef Q_OS_LINUX
    esLinux = true;
#else
    esLinux = false;
#endif
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionCalibracion_triggered()
{
    qbaSalida.clear();
    qbaEntrada->clear();
    qbaEntrada->append(ui->plainTextEdit->toPlainText().toLatin1());
    qbaSalida = trayectoria->trazarCalibracion();
    if (crearArchivos(true)){
        ui->statusbar->showMessage("Se ha generado el proyecto de calibración.",10000);
    }
}


void MainWindow::on_actionGenera_triggered()
{
    qbaSalida.clear();
    qbaEntrada->clear();
    qbaEntrada->append(ui->plainTextEdit->toPlainText().toLatin1());
    qbaSalida = trayectoria->trazar(qbaEntrada);
    if (guardarProyecto()){
        ui->statusbar->showMessage("Se ha generado el proyecto " + ui->lineEditNombreProyecto->text() + ".",10000);
    }
}

void MainWindow::on_actionInformacion_triggered()
{
    if (!Info::estaInstanciada()){
        info = new Info(this);
        info->setAttribute(Qt::WA_DeleteOnClose);
        info->show();
    }
}

bool MainWindow::guardarProyecto()
{
    if (ui->lineEditNombreProyecto->text() == ""){
        QMessageBox::information(nullptr, "Faltó algo...",
                                 "El proyecto necesita un nombre.",
                                 QMessageBox::Ok);
        return false;
    }
    if (ui->plainTextEdit->toPlainText() == ""){
        QMessageBox::StandardButton boton;
        boton = QMessageBox::information(nullptr, "¿Y nada más?",
                                         "¿Deseas continuar generando el proyecto con tan poco escrito?",
                                         QMessageBox::Ok | QMessageBox::Cancel);
        if (boton == QMessageBox::Cancel){
            return false;
        }
    }
    return crearArchivos();
}

bool MainWindow::crearArchivos(const bool esCalibracion)
{
    QString directorio = QFileDialog::getExistingDirectory(this, tr("Directorio para guardar el proyecto..."), "/home",
                                                           QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    QDir dir(directorio);
    if (!dir.isEmpty()){
        QMessageBox::StandardButton boton;
        boton = QMessageBox::information(nullptr, "Guardar",
                                         "La carpeta no está vacía.\n"
                                         "Se sobreescribirán los archivos que tengan el mismo nombre.\n"
                                         "¿Deseas guardarlo ahí igualmente?",
                                         QMessageBox::Ok | QMessageBox::Cancel);
        if (boton == QMessageBox::Cancel){
            return false;
        }
    }
    if (!directorio.isEmpty()){
        QFile archivoModule, archivoPGF, archivoCalibData;

        if (esCalibracion){
            directorio.append("/Calibracion");
            dir.mkpath(directorio);
            archivoPGF.setFileName(directorio + "/TranscribotCalibracion.pgf");
        }
        else{
            directorio.append("/" + ui->lineEditNombreProyecto->text());
            dir.mkpath(directorio);
            archivoPGF.setFileName(directorio + "/" + ui->lineEditNombreProyecto->text() + ".pgf");
        }
        archivoModule.setFileName(directorio + "/Module1.mod");
        archivoCalibData.setFileName(directorio + "/CalibData.mod");

        if(archivoModule.open(QIODevice::WriteOnly | QIODevice::Text)){
            QTextStream salArchivo(&archivoModule);
            salArchivo.setCodec("UTF-8");
            salArchivo << correccionSaltoLinea(qbaSalida);
            archivoModule.close();
        }

        if(archivoPGF.open(QIODevice::WriteOnly | QIODevice::Text)){
            QTextStream salArchivo(&archivoPGF);
            salArchivo.setCodec("UTF-8");
            salArchivo << correccionSaltoLinea(contenidoPGF);
            archivoPGF.close();
        }

        if(archivoCalibData.open(QIODevice::WriteOnly | QIODevice::Text)){
            QTextStream salArchivo(&archivoCalibData);
            salArchivo.setCodec("UTF-8");
            salArchivo << correccionSaltoLinea(contenidoCalibData);
            archivoCalibData.close();
        }
        return true;
    }
    return false;
}

QByteArray MainWindow::correccionSaltoLinea(const QByteArray &qba)
{
    if (esLinux){
        QByteArray qbaCorregido;
        char c;
        for (int i = 0; i < qba.size(); i++){
            c = qba.at(i);
            if (c == '\n') {qbaCorregido.push_back("\r\n");}
            else {qbaCorregido.push_back(c);}
        }
        return qbaCorregido;
    }
    return qba;
}

void MainWindow::on_actionEjemplos_triggered()
{
    if (!Ejemplo::estaInstanciada()){
        ejemplo = new Ejemplo(this);
        connect(ejemplo, SIGNAL(textoEjemplo(QString)), this, SLOT(setTextoEjemplo(QString)));
        ejemplo->setAttribute(Qt::WA_DeleteOnClose);
        ejemplo->show();
    }
}

void MainWindow::setTextoEjemplo(QString texto)
{
    ui->plainTextEdit->clear();
    ui->plainTextEdit->setPlainText(texto);
}

void MainWindow::on_actionAyuda_triggered()
{
    if (!Ayuda::estaInstanciada()){
        ayuda = new Ayuda(this);
        ayuda->setAttribute(Qt::WA_DeleteOnClose);
        ayuda->show();
    }
}

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ayuda.cpp \
    ejemplo.cpp \
    info.cpp \
    main.cpp \
    mainwindow.cpp \
    tecla.cpp \
    trayectoria.cpp

HEADERS += \
    ayuda.h \
    ejemplo.h \
    info.h \
    mainwindow.h \
    tecla.h \
    trayectoria.h

FORMS += \
    ayuda.ui \
    ejemplo.ui \
    info.ui \
    mainwindow.ui

TRANSLATIONS += \
    transcribot_es_ES.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    rsc.qrc

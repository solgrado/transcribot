/****************************************************************************
**
** Copyright (C) 2021 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Transcribot.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <trayectoria.h>
#include <info.h>
#include <ejemplo.h>
#include <ayuda.h>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionCalibracion_triggered();
    void on_actionGenera_triggered();
    void on_actionInformacion_triggered();
    void on_actionEjemplos_triggered();
    void setTextoEjemplo(QString texto);

    void on_actionAyuda_triggered();

private:
    Ui::MainWindow *ui;

    Trayectoria *trayectoria;
    QByteArray *qbaEntrada;
    QByteArray qbaSalida;
    Info *info;
    Ejemplo *ejemplo;
    Ayuda *ayuda;
    bool esLinux;

    bool guardarProyecto();
    bool crearArchivos(const bool esCalibracion = false);
    QByteArray correccionSaltoLinea(const QByteArray &qba);
};


#endif // MAINWINDOW_H

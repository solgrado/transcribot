#include "ayuda.h"
#include "ui_ayuda.h"

Ayuda::Ayuda(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Ayuda)
{
    ui->setupUi(this);
    _hayInstancia = true;
}

Ayuda::~Ayuda()
{
    _hayInstancia = false;
    delete ui;
}

bool Ayuda::_hayInstancia = false;

bool Ayuda::estaInstanciada()
{
    return _hayInstancia;
}

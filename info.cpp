#include "info.h"
#include "ui_info.h"

Info::Info(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Info)
{
    ui->setupUi(this);
    _hayInstancia = true;
}

Info::~Info()
{
    _hayInstancia = false;
    delete ui;
}

bool Info::_hayInstancia = false;

bool Info::estaInstanciada()
{
    return _hayInstancia;
}

#include "trayectoria.h"


Trayectoria::Trayectoria(QObject *parent) : QObject(parent)
{
    trayecto = new QByteArray;
    tecla = new Tecla;
    velocidadSoltar = "v100";
    velocidadPulsar = "v100";
    precisionPulsar = "fine";
    precisionSoltar = "fine";
    velocidadDespLibre = "v200";
    cabecera =  "MODULE Module1\n"
                "\tCONST robtarget Inicio:=[[468.276877527,0,534],[0.5,0,0.866025404,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_49:=[[449.944,190.056,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_50:=[[450,171,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_51:=[[450,152,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_52:=[[450,133,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_53:=[[450,114,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_54:=[[450,95,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_55:=[[450,76,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_56:=[[450,57,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_57:=[[450,38,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_97:=[[412.5,174.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_96:=[[431.25,-9.5,18],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_98:=[[393.75,89,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_99:=[[393.75,127,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_44:=[[393.75,32,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_100:=[[412.5,136.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_101:=[[431.25,142.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_241:=[[412.5,3.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_13:=[[423.885,-53.838,18],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_32:=[[375,89,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_102:=[[412.5,117.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_103:=[[412.5,98.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_45:=[[393.75,-6,16],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_104:=[[412.5,79.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_105:=[[431.25,47.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_106:=[[412.5,60.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_107:=[[412.5,41.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_108:=[[412.5,22.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_109:=[[393.75,51,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_256:=[[412.5,201,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_110:=[[393.75,70,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_111:=[[431.25,28.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_112:=[[431.25,9.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_46:=[[393.75,13,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_113:=[[431.25,180.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_114:=[[431.25,123.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_115:=[[412.5,155.5,17],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_116:=[[431.25,104.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_257:=[[412.5,-15.5,17],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_117:=[[431.25,66.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_118:=[[393.75,108,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_119:=[[431.25,161.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_120:=[[393.75,146,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_121:=[[431.25,85.5,18],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_122:=[[393.733,165.017,16],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_48:=[[450,19,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_176:=[[450,209,20],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_161:=[[450,-19,20],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_63:=[[450,0,20],[0,0,1,0],[0,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"

                "\tCONST robtarget Target_49_Pulsado:=[[449.944,190.056,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_50_Pulsado:=[[450,171,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_51_Pulsado:=[[450,152,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_52_Pulsado:=[[450,133,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_53_Pulsado:=[[450,114,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_54_Pulsado:=[[450,95,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_55_Pulsado:=[[450,76,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_56_Pulsado:=[[450,57,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_57_Pulsado:=[[450,38,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_97_Pulsado:=[[412.5,174.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_96_Pulsado:=[[431.25,-9.5,13],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_98_Pulsado:=[[393.75,89,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_99_Pulsado:=[[393.75,127,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_48_Pulsado:=[[450,19,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_44_Pulsado:=[[393.75,32,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_100_Pulsado:=[[412.5,136.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_101_Pulsado:=[[431.25,142.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_241_Pulsado:=[[412.5,3.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_13_Pulsado:=[[423.885,-53.838,13],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_32_Pulsado:=[[375,89,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_102_Pulsado:=[[412.5,117.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_103_Pulsado:=[[412.5,98.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_176_Pulsado:=[[450,209,15],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_45_Pulsado:=[[393.75,-6,11],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_104_Pulsado:=[[412.5,79.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_105_Pulsado:=[[431.25,47.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_161_Pulsado:=[[450,-19,15],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_63_Pulsado:=[[450,0,15],[0,0,1,0],[0,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_106_Pulsado:=[[412.5,60.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_107_Pulsado:=[[412.5,41.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_108_Pulsado:=[[412.5,22.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_109_Pulsado:=[[393.75,51,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_256_Pulsado:=[[412.5,201,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_110_Pulsado:=[[393.75,70,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_111_Pulsado:=[[431.25,28.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_112_Pulsado:=[[431.25,9.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_46_Pulsado:=[[393.75,13,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_113_Pulsado:=[[431.25,180.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_114_Pulsado:=[[431.25,123.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_115_Pulsado:=[[412.5,155.5,12],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_116_Pulsado:=[[431.25,104.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_257_Pulsado:=[[412.5,-15.5,12],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_117_Pulsado:=[[431.25,66.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_118_Pulsado:=[[393.75,108,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_119_Pulsado:=[[431.25,161.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_120_Pulsado:=[[393.75,146,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_121_Pulsado:=[[431.25,85.5,13],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_122_Pulsado:=[[393.733,165.017,11],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"

                "\tCONST robtarget Target_49_Elevado:=[[449.944,190.056,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_50_Elevado:=[[450,171,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_51_Elevado:=[[450,152,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_52_Elevado:=[[450,133,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_53_Elevado:=[[450,114,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_54_Elevado:=[[450,95,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_55_Elevado:=[[450,76,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_56_Elevado:=[[450,57,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_57_Elevado:=[[450,38,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_97_Elevado:=[[412.5,174.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_96_Elevado:=[[431.25,-9.5,28],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_98_Elevado:=[[393.75,89,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_99_Elevado:=[[393.75,127,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_48_Elevado:=[[450,19,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_44_Elevado:=[[393.75,32,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_100_Elevado:=[[412.5,136.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_101_Elevado:=[[431.25,142.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_241_Elevado:=[[412.5,3.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_13_Elevado:=[[423.885,-53.838,28],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_32_Elevado:=[[375,89,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_102_Elevado:=[[412.5,117.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_103_Elevado:=[[412.5,98.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_176_Elevado:=[[450,209,30],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_45_Elevado:=[[393.75,-6,26],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_104_Elevado:=[[412.5,79.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_105_Elevado:=[[431.25,47.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_161_Elevado:=[[450,-19,30],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_63_Elevado:=[[450,0,30],[0,0,1,0],[0,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_106_Elevado:=[[412.5,60.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_107_Elevado:=[[412.5,41.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_108_Elevado:=[[412.5,22.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_109_Elevado:=[[393.75,51,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_256_Elevado:=[[412.5,201,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_110_Elevado:=[[393.75,70,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_111_Elevado:=[[431.25,28.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_112_Elevado:=[[431.25,9.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_46_Elevado:=[[393.75,13,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_113_Elevado:=[[431.25,180.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_114_Elevado:=[[431.25,123.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_115_Elevado:=[[412.5,155.5,27],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_116_Elevado:=[[431.25,104.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_257_Elevado:=[[412.5,-15.5,27],[0,0,1,0],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_117_Elevado:=[[431.25,66.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_118_Elevado:=[[393.75,108,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_119_Elevado:=[[431.25,161.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_120_Elevado:=[[393.75,146,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_121_Elevado:=[[431.25,85.5,28],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\tCONST robtarget Target_122_Elevado:=[[393.733,165.017,26],[0,0,1,0],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];\n"
                "\t!***********************************************************\n"
                "\t!\n"
                "\t! Módulo:  Module1\n"
                "\t!\n"
                "\t! Descripción:\n"
                "\t!   <Introduzca la descripción aquí>\n"
                "\t!\n"
                "\t! Autor: TranscriBot v0.2beta\n"
                "\t!\n"
                "\t! Versión: 1.0\n"
                "\t!\n"
                "\t!***********************************************************\n"
                "\t!\n"
                "\t! Procedimiento Main\n"
                "\t!\n"
                "\t!   Este es el punto de entrada de su programa\n"
                "\t!\n"
                "\t!***********************************************************\n"
                "\tPROC main()\n"
                "\t\tPath_Teclear;\n"
                "\tENDPROC\n"
                "\tPROC Path_Teclear()\n";
}

QByteArray Trayectoria::trazar(QByteArray *texto){
    QString caracter;
    int n_tecla, tipoPulsacion;
    trayecto->clear();
    trayecto->push_back(cabecera);
    trayecto->push_back("\t\tMoveJ Inicio,v400,z100,MyNewTool\\WObj:=wobj0;\n");
    for(int i = 0; i < texto->size(); i++){
        caracter = texto->at(i);
        tecla->getTipoPulsacion(caracter,n_tecla,tipoPulsacion);
        if (tipoPulsacion == PULSACION_SIMPLE){
            agregarPulsacion(n_tecla);
        }
        else if(tipoPulsacion == PULSACION_CON_MAYUSCULA){
            agregarPulsacion(BLOQ_MAYUSCULAS);
            agregarPulsacion(n_tecla);
            agregarPulsacion(BLOQ_MAYUSCULAS);
        }
        else if (tipoPulsacion == PULSACION_CON_TILDE){
            agregarPulsacion(TILDE);
            agregarPulsacion(n_tecla);
        }
        else if (tipoPulsacion == PULSACION_CON_MAYUSCULA_Y_TILDE){
            agregarPulsacion(BLOQ_MAYUSCULAS);
            agregarPulsacion(TILDE);
            agregarPulsacion(n_tecla);
            agregarPulsacion(BLOQ_MAYUSCULAS);
        }
        else{
            //Y siiii...
        }
    }
    trayecto->push_back("\t\tMoveJ Inicio,v400,z20,MyNewTool\\WObj:=wobj0;\n"
                        "\tENDPROC\n"
                        "ENDMODULE");
    return *trayecto;
}

QByteArray Trayectoria::trazarCalibracion()
{
    trayecto->clear();
    trayecto->push_back(cabecera);
    trayecto->push_back("\t\tMoveJ Inicio,v400,z20,MyNewTool\\WObj:=wobj0;\n");
            agregarPulsacion(GRADO, true);
            agregarPulsacion(EXCLAMACION, true);
            agregarPulsacion(B, true);
    trayecto->push_back("\t\tMoveJ Inicio,v400,z20,MyNewTool\\WObj:=wobj0;\n"
                        "\tENDPROC\n"
                        "ENDMODULE");
    return *trayecto;
}

void Trayectoria::clear()
{
    trayecto->clear();
}

void Trayectoria::agregarElevado(const int &n_tecla, const bool esDespLibre, const bool esCalibracion)
{
    trayecto->push_back("\t\tMoveL Target_");
    trayecto->push_back(QString::number(n_tecla).toLatin1());
    trayecto->push_back("_Elevado,");

    if (esCalibracion){
        trayecto->push_back("v100,");
        trayecto->push_back("fine,");
    }
    else if(esDespLibre){
        trayecto->push_back(velocidadDespLibre.toLatin1() + ",");
        trayecto->push_back(precisionSoltar.toLatin1() + ",");
    }
    else{
        trayecto->push_back(velocidadSoltar.toLatin1() + ",");
        trayecto->push_back(precisionSoltar.toLatin1() + ",");
    }
    trayecto->push_back("MyNewTool\\WObj:=wobj0;\n");
}

void Trayectoria::agregarPulsado(const int &n_tecla, const bool esCalibracion)
{
    trayecto->push_back("\t\tMoveL Target_");
    trayecto->push_back(QString::number(n_tecla).toLatin1());

    if (esCalibracion){
        trayecto->push_back(",v50,");
        trayecto->push_back("fine,");
    }
    else{
        trayecto->push_back("_Pulsado,");
        trayecto->push_back(velocidadPulsar.toLatin1() + ",");
        trayecto->push_back(precisionPulsar.toLatin1() + ",");
    }
    trayecto->push_back("MyNewTool\\WObj:=wobj0;\n");
}

void Trayectoria::agregarPulsacion(const int &n_tecla, const bool calibracion)
{
    agregarElevado(n_tecla, true, calibracion); //Desplazamiento libre entre teclas
    agregarPulsado(n_tecla, calibracion);
    agregarElevado(n_tecla, false, calibracion);
}


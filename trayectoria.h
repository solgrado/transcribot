/****************************************************************************
**
** Copyright (C) 2021 Santi O.L.(Solgrado)
** <solgrado@protonmail.com>
**
** Este archivo forma parte del proyecto Transcribot.
**
** Este programa es software libre: puede redistribuirlo y/o modificarlo bajo
** los términos de la Licencia Pública General de GNU según se encuentra
** publicada por la Free Software Foundation, bien de la versión 3 de dicha
** Licencia o bien (según su elección) de cualquier versión posterior.
**
** Este programa se distribuye con la esperanza de que sea útil, pero
** SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita ni la de
** garantizar la ADECUACIÓN A UN PROPÓSITO PARTICULAR. Véase la Licencia
** Pública General de GNU para más detalles.
**
** Debería haber recibido una copia de la Licencia Pública General  de GNU
** junto con este programa. Si no ha sido así, consulte:
** http://www.gnu.org/licenses/.
**
****************************************************************************/

#ifndef TRAYECTORIA_H
#define TRAYECTORIA_H

#include <QObject>
#include <tecla.h>

class Trayectoria : public QObject
{
    Q_OBJECT
public:
    explicit Trayectoria(QObject *parent = nullptr);

    QByteArray trazar(QByteArray *texto);
    QByteArray trazarCalibracion();
    void clear();

signals:

public slots:
    void setVelDespLibre(QString vel){velocidadDespLibre = vel;}
    void setVelPulsar(QString vel){velocidadPulsar = vel;}
    void setVelSoltar(QString vel){velocidadSoltar = vel;}
    void setPrecPulsar(QString prec){precisionPulsar = prec;}
    void setPrecSoltar(QString prec){precisionSoltar = prec;}

private:
    Tecla *tecla;
    QByteArray cabecera;
    QByteArray *trayecto;
    QString velocidadSoltar;
    QString velocidadPulsar;
    QString precisionPulsar;
    QString precisionSoltar;
    QString velocidadDespLibre;

    void agregarElevado(const int &n_tecla, const bool esDespLibre, const bool calibracion = 0);
    void agregarPulsado(const int &n_tecla, const bool calibracion = 0);
    void agregarPulsacion(const int &n_tecla, const bool calibracion = 0);
 };


#endif // TRAYECTORIA_H

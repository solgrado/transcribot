Este programa ha sido creado durante el curso de Mecatrónica industrial 2020/2021, en el IES Punta Candieira.

![Captura Transcribot](https://solgrado.gitlab.io/recursos/transcribot_screenshot.png)

Transcribot genera un proyecto para un brazo robótico ABB partiendo de un texto personalizado. Estos proyectos incluyen los archivos necesarios para que el brazo robot, mediante las instrucciones y trayectorias incluidas, escriba dicho texto personalizado en un teclado de ordenador común.

Dedicado a los/la compañeros/a con las que he compartido angustias antes de los exámenes y a las/los profesoras/es, que se esforzaron en difundir conocimiento.

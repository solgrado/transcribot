#include "tecla.h"

Tecla::Tecla(QObject *parent) : QObject(parent)
{

}

void Tecla::getTipoPulsacion(QString caracter, int &tecla, int &tipoPulsacion)
{
    if (caracter == "") {tecla = 0; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "a")  {tecla = 97; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "b")  {tecla = 98; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "c")  {tecla = 99; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "d")  {tecla = 100; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "e")  {tecla = 101; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "f")  {tecla = 102; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "g")  {tecla = 103; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "h")  {tecla = 104; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "i")  {tecla = 105; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "j")  {tecla = 106; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "k")  {tecla = 107; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "l")  {tecla = 108; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "m")  {tecla = 109; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "n")  {tecla = 110; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "ñ")  {tecla = 241; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "o")  {tecla = 111; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "p")  {tecla = 112; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "q")  {tecla = 113; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "r")  {tecla = 114; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "s")  {tecla = 115; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "t")  {tecla = 116; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "u")  {tecla = 117; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "v")  {tecla = 118; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "w")  {tecla = 119; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "x")  {tecla = 120; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "y")  {tecla = 121; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "z")  {tecla = 122; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "0")  {tecla = 48; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "1")  {tecla = 49; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "2")  {tecla = 50; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "3")  {tecla = 51; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "4")  {tecla = 52; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "5")  {tecla = 53; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "6")  {tecla = 54; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "7")  {tecla = 55; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "8")  {tecla = 56; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "9")  {tecla = 57; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == ",")  {tecla = 44; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == ".")  {tecla = 46; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "-")  {tecla = 45; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "¡")  {tecla = 161; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == " ")  {tecla = 32; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "\n") {tecla = 13; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "\'") {tecla = 63; tipoPulsacion = PULSACION_SIMPLE;}
    else if (caracter == "º") {tecla = 176; tipoPulsacion = PULSACION_SIMPLE;}

    else if (caracter == "A")  {tecla = 97; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "B")  {tecla = 98; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "C")  {tecla = 99; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "D")  {tecla = 100; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "E")  {tecla = 101; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "F")  {tecla = 102; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "G")  {tecla = 103; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "H")  {tecla = 104; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "I")  {tecla = 105; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "J")  {tecla = 106; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "K")  {tecla = 107; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "L")  {tecla = 108; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "M")  {tecla = 109; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "N")  {tecla = 110; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "Ñ")  {tecla = 241; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "O")  {tecla = 111; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "P")  {tecla = 112; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "Q")  {tecla = 113; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "R")  {tecla = 114; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "S")  {tecla = 115; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "T")  {tecla = 116; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "U")  {tecla = 117; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "V")  {tecla = 118; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "W")  {tecla = 119; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "X")  {tecla = 120; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "Y")  {tecla = 121; tipoPulsacion = PULSACION_CON_MAYUSCULA;}
    else if (caracter == "Z")  {tecla = 122; tipoPulsacion = PULSACION_CON_MAYUSCULA;}


    else if (caracter == "á")  {tecla = 97; tipoPulsacion = PULSACION_CON_TILDE;}
    else if (caracter == "é")  {tecla = 101; tipoPulsacion = PULSACION_CON_TILDE;}
    else if (caracter == "í")  {tecla = 105; tipoPulsacion = PULSACION_CON_TILDE;}
    else if (caracter == "ó")  {tecla = 111; tipoPulsacion = PULSACION_CON_TILDE;}
    else if (caracter == "ú")  {tecla = 117; tipoPulsacion = PULSACION_CON_TILDE;}


    else if (caracter == "Á")  {tecla = 97; tipoPulsacion = PULSACION_CON_MAYUSCULA_Y_TILDE;}
    else if (caracter == "É")  {tecla = 101; tipoPulsacion = PULSACION_CON_MAYUSCULA_Y_TILDE;}
    else if (caracter == "Í")  {tecla = 105; tipoPulsacion = PULSACION_CON_MAYUSCULA_Y_TILDE;}
    else if (caracter == "Ó")  {tecla = 111; tipoPulsacion = PULSACION_CON_MAYUSCULA_Y_TILDE;}
    else if (caracter == "Ú")  {tecla = 117; tipoPulsacion = PULSACION_CON_MAYUSCULA_Y_TILDE;}

    else{tipoPulsacion = -1;}
}


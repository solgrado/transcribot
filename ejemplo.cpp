#include "ejemplo.h"
#include "ui_ejemplo.h"

Ejemplo::Ejemplo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Ejemplo)
{
    ui->setupUi(this);
    _hayInstancia = true;
}

Ejemplo::~Ejemplo()
{
    _hayInstancia = false;
    delete ui;
}

bool Ejemplo::_hayInstancia = false;

bool Ejemplo::estaInstanciada()
{
    return _hayInstancia;
}

void Ejemplo::on_pButtonAceptar_clicked()
{
    QString texto;
    QString titulo = ui->listWidgetEjemplos->currentItem()->text();

    if (titulo == "A xusticia pola man - Rosalía de Castro"){
        texto = "A xusticia pola man - Rosalía de Castro\n"
                "\n"
                "\n"
                "Aqués que tén fama d' honrados na vila\n"
                "roubáronme tanta brancura qu' eu tiña;\n"
                "botáronme estrume nas galas dun día,\n"
                "a roupa de cote puñéronma en tiras.\n"
                "\n"
                "Nin pedra deixaron en dond' eu vivira;\n"
                "sin lar, sin abrigo, morei nas curtiñas;\n"
                "ó raso cas lebres dormín nas campías;\n"
                "meus fillos... ¡meus anxos!... que tant' eu quería,\n"
                "¡morreron, morreron ca fame que tiñan!.\n"
                "\n"
                "Quedei deshonrada, mucháronm' a vida,\n"
                "fixéronm' un leito de toxos e silvas;\n"
                "i en tanto, os raposos de sangre maldita,\n"
                "tranquilos nun leito de rosas dormían.\n"
                "\n"
                "―Salvádeme ¡ouh, xueces!, berrei... ¡Tolería!\n"
                "De min se mofaron, vendeum' a xusticia.\n"
                "―Bon Dios, axudaime, berrei, berrei inda...\n"
                "tan alto qu' estaba, bon Dios non m' oíra.\n"
                "\n"
                "Estonces, cal loba doente ou ferida,\n"
                "dun salto con rabia pillei a fouciña,\n"
                "rondei paseniño... (ne' as herbas sentían)\n"
                "i a lúa escondíase, i a fera dormía\n"
                "cos seus compañeiros en cama mullida.\n"
                "\n"
                "Mireinos con calma, i as mans estendidas,\n"
                "dun golpe ¡dun soio! deixeinos sin vida.\n"
                "I ó lado, contenta, senteime das vítimas,\n"
                "tranquila, esperando pola alba do día.\n"
                "\n"
                "I estonces... estonces cumpreuse a xusticia:\n"
                "eu, neles; i as leises, na man qu' os ferira.\n";
        emit textoEjemplo(texto);
    }

    else if (titulo == "A raposa e o galo - Conto popular galego"){
        texto = "A raposa e o galo - Conto popular galego\n"
                "\n"
                "\n"
                "Unha raposa pillou un galo e ía con el por unha leira arriba. O galo ía dicindo sen parar:\n"
                "\n"
                "— ¡Xeou! ¡Xeou!\n"
                "\n"
                "A raposa aborreceuse de oílo sempre coa mesma cantinela e foi e díxolle:\n"
                "\n"
                "—Se xeou que xease.\n"
                "\n"
                "Ao abrír a boca a raposa o galo voou para un carballo quedando a raposa máis amolada ca se lle arrincaran o rabo."
                " Deu unhas cantas voltas arredor do rebolo e, despois, púxose a fregar o rabo contra el coma se fose un serrón, dicindo:\n"
                "\n"
                "—Corta, rabo, comerás galo. Corta, rabo, comerás galo...\n"
                "\n"
                "Mais o galo non voou do rebolo embaixo co medo de que a raposa o cortase, que era o que ela buscaba, senón que se puxo a cantar:\n"
                "\n"
                "—Deus che me libre de fouce ou machado que rabo de raposa non corta carballo.\n";
        emit textoEjemplo(texto);
    }

    else if (titulo == "O rato do monte e o da casa - Conto popular galego"){
        texto = "O rato do monte e o da casa - Conto popular galego\n"
                "\n"
                "\n"
                "Saíu unha vez un rato da casa nun día de inverno e encontrou un rato de monte, cheo de frío e moi fraco.\n"
                "\n"
                "—E ti, ¿como estás así tan fraco? —díxolle o da casa—. Vente comigo, que alí onde eu estou hai sempre que comer, sen necesidades de pasar frío nin de mollarse para buscar que comer.\n"
                "\n"
                "—¿E o gato? —preguntou o do monte.\n"
                "\n"
                "—¡Boh! —respondeulle  o  da  casa—.  Hai  alí  un,  pero  é coma se nada. Sempre está durmindo. Ti ven, e verás como paso por diante del e nin sequera me ve.\n"
                "\n"
                "Foron os dous para casa e viron o gato durmido diante da porta. Pasou o rato por diante del todo confiado pero o gato  abriu  un  pouco  os  ollos,  deu  un  brinco  e o  rato quedou entre as uñas e os dentes do gato.\n"
                "\n"
                "O rato do monte, que tal viu, deu media volta e dixo:\n"
                "\n"
                "—¡Vale máis no monte fraco que gordo no cu do gato!\n";
        emit textoEjemplo(texto);
    }


    else if (titulo == "A galiña asustada - Conto popular galego"){
        texto = "A galiña asustada - Conto popular galego\n"
                "\n"
                "\n"
                "Unha vez era unha pita que estaba debaixo dunha pereira e caíulle pera na coroniña. Marchou onde o galo e díxolle:\n"
                "\n"
                "-Marche de aí, don Galo, que cae o mundo a pedazos.\n"
                "\n"
                "-Quen llo dixo, doña Galiña?\n"
                "\n"
                "-Caeume a min na coroniña\n"
                "\n"
                "E marchou. Chegou onde a lebre e díxolle:\n"
                "\n"
                "-Marche de aí, doña Lebre, que cae o mundo a pedazos.\n"
                "\n"
                "-Quen llo dixo, doña Galiña?\n"
                "\n"
                "-Caeume a min na coroniña.\n"
                "\n"
                "Marchou e chegou onde a raposa:\n"
                "\n"
                "-Marche de aí doña Raposa, que cae o mundo a pedazos.\n"
                "\n"
                "-Quen llo dixo doña Galiña?\n"
                "\n"
                "-Caeume a min na coroniña.\n"
                "\n"
                "-Se cae o mundo a pedazos, eu quero ir ben fartiña.\n"
                "\n"
                "E chapou á faladora galiña.\n";
        emit textoEjemplo(texto);
    }


    else if (titulo == "Agora si que te collín! - Conto popular galego"){
        texto = "Agora si que te collín! - Conto popular galego\n"
                "\n"
                "\n"
                "Un pequeno máis listo ca Benito sabía o catecismo do dereito e do revés. Tan ben, que el mesmo lle dicía ao señor abade que non tiña por onde collelo.\n"
                " Un día, o señor abade empezoulle a preguntar polo máis difícil. Pero o rapaz non fallaba unha. Seguíulle preguntando e, por fin, dunha vez o pequeno non soubo responder.\n"
                " Entón o crego empezoulle a dicir:\n"
                "\n"
                "—Logo caíches, rapaz. E logo caíches.\n"
                "\n"
                "Quedouse o rapaz todo avergoñado pola burla e quedouse pensando como cobrarlla ao crego. Por fin ocorréuselle preguntarlle:\n"
                "\n"
                "—Señor abade, e Deus estará no forno do meu pai?\n"
                "\n"
                "—Home! E logo? Ti non sabes que Deus está en todas partes?\n"
                "\n"
                "—Agora si que tamén o collín eu a vostede, señor abade, que meu pai non ten forno.\n";
        emit textoEjemplo(texto);
    }


    else if (titulo == "Bella Ciao - Canción partisana"){
        texto = "Bella Ciao - Canción partisana"
                "\n"
                "\n"
                "Esta mañana me he levantado,\n"
                "oh bella ciao, bella ciao, bella ciao, ciao, ciao!\n"
                "Esta mañana me he levantado\n"
                "y he descubierto al invasor.\n"
                "\n"
                "Oh compañero, quiero ir contigo,\n"
                "oh bella ciao, bella ciao, bella ciao, ciao, ciao\n"
                "oh compañero, quiero ir contigo.\n"
                "Porque me siento aquí morir.\n"
                "\n"
                "Y si muero como partisano,\n"
                "oh bella ciao, bella ciao, bella ciao, ciao, ciao\n"
                "y si muero como partisano\n"
                "entonces me deberás enterrar.\n"
                "\n"
                "Entiérrame en la montaña,\n"
                "oh bella ciao, bella ciao, bella ciao, ciao, ciao\n"
                "entiérrame en la montaña\n"
                "bajo la sombra de una bella flor.\n"
                "\n"
                "Y todos aquellos que pasen,\n"
                "oh bella ciao, bella ciao, bella ciao, ciao, ciao\n"
                "y todos aquellos que pasen\n"
                "me dirán 'que bella flor'\n"
                "\n"
                "Ésta es la flor del partisano,\n"
                "oh bella ciao, bella ciao, bella ciao, ciao, ciao\n"
                "Ésta es la flor del partisano\n"
                "que murió por la libertad.\n";
        emit textoEjemplo(texto);
    }


    else if (titulo == "En el pozo María Luisa - Canción popular minera"){
        texto ="En el pozo María Luisa - Canción popular minera\n"
               "\n"
               "\n"
               "En el pozo María Luisa,\n"
               "tranlaralará, tranlará, tranlará.\n"
               "murieron cuatro mineros.\n"
               "Mira, mira Maruxina mira,\n"
               "mira como vengo yo.\n"
               "murieron cuatro mineros.\n"
               "Mira, mira Maruxina mira,\n"
               "mira como vengo yo.\n"
               "\n"
               "Traigo la camisa roja\n"
               "tranlaralará, tranlará, tranlará.\n"
               "de sangre de un compañero.\n"
               "Mira, mira Maruxina mira,\n"
               "mira como vengo yo.\n"
               "de sangre de un compañero.\n"
               "Mira, mira Maruxina mira,\n"
               "mira como vengo yo.\n"
               "\n"
               "Traigo la cabeza rota,\n"
               "tranlaralará, tranlará, tranlará.\n"
               "que me la rompió un costero.\n"
               "Mira, mira Maruxina mirai,\n"
               "mirai como vengo yo.\n"
               "que me la rompió un barreno.\n"
               "Mira, mira Maruxina mira,\n"
               "mira como vengo yo.\n"
               "\n"
               "Santa Bárbara bendita,\n"
               "tranlaralará, tranlará, tranlará.\n"
               "Patrona de los mineros.\n"
               "Mirad, mirad Maruxina mirad,\n"
               "mira como vengo yo.\n"
               "Patrona de los mineiros.\n"
               "Mirad, mira Maruxina mira,\n"
               "mira como vengo yo.\n"
               "\n"
               "Mañana son los entierros,\n"
               "tranlaralará, tranlará, tranlará,\n"
               "de esos pobres compañeros,\n"
               "Mira, mira Maruxina mira,\n"
               "mira como vengo yo.\n"
               "de esos pobres compañeros,\n"
               "Mira, mira Maruxina mira,\n"
               "mira como vengo yo.\n";
        emit textoEjemplo(texto);
    }

    else if (titulo == "Nanas de la cebolla - Miguel Hernández"){
        texto = "Nanas de la cebolla - Miguel Hernández\n"
                "\n"
                "\n"
                "La cebolla es escarcha\n"
                "cerrada y pobre.\n"
                "Escarcha de tus días\n"
                "y de mis noches.\n"
                "Hambre y cebolla,\n"
                "hielo negro y escarcha\n"
                "grande y redonda.\n"
                "\n"
                "En la cuna del hambre\n"
                "mi niño estaba.\n"
                "Con sangre de cebolla\n"
                "se amamantaba.\n"
                "Pero tu sangre,\n"
                "escarchada de azúcar,\n"
                "cebolla y hambre.\n"
                "\n"
                "Una mujer morena\n"
                "resuelta en luna\n"
                "se derrama hilo a hilo\n"
                "sobre la cuna.\n"
                "Ríete, niño,\n"
                "que te tragas la luna\n"
                "cuando es preciso.\n"
                "\n"
                "Alondra de mi casa,\n"
                "ríete mucho.\n"
                "Es tu risa en tus ojos\n"
                "la luz del mundo.\n"
                "Ríete tanto\n"
                "que en el alma al oírte\n"
                "bata el espacio.\n"
                "\n"
                "Tu risa me hace libre,\n"
                "me pone alas.\n"
                "Soledades me quita,\n"
                "cárcel me arranca.\n"
                "Boca que vuela,\n"
                "corazón que en tus labios\n"
                "relampaguea.\n"
                "\n"
                "Es tu risa la espada\n"
                "más victoriosa,\n"
                "vencedor de las flores\n"
                "y las alondras.\n"
                "Rival del sol.\n"
                "Porvenir de mis huesos\n"
                "y de mi amor.\n"
                "\n"
                "La carne aleteante,\n"
                "súbito el párpado,\n"
                "el niño como nunca\n"
                "coloreado.\n"
                "¡Cuánto jilguero\n"
                "se remonta, aletea,\n"
                "desde tu cuerpo!\n"
                "\n"
                "Desperté de ser niño:\n"
                "nunca despiertes.\n"
                "Triste llevo la boca:\n"
                "ríete siempre.\n"
                "Siempre en la cuna,\n"
                "defendiendo la risa\n"
                "pluma por pluma.\n"
                "\n"
                "Ser de vuelo tan alto,\n"
                "tan extendido,\n"
                "que tu carne es el cielo\n"
                "recién nacido.\n"
                "¡Si yo pudiera\n"
                "remontarme al origen\n"
                "de tu carrera!\n"
                "\n"
                "Al octavo mes ríes\n"
                "con cinco azahares.\n"
                "Con cinco diminutas\n"
                "ferocidades.\n"
                "Con cinco dientes\n"
                "como cinco jazmines\n"
                "adolescentes.\n"
                "\n"
                "Frontera de los besos\n"
                "serán mañana,\n"
                "cuando en la dentadura\n"
                "sientas un arma.\n"
                "Sientas un fuego\n"
                "correr dientes abajo\n"
                "buscando el centro.\n"
                "\n"
                "Vuela niño en la doble\n"
                "luna del pecho:\n"
                "él, triste de cebolla,\n"
                "tú, satisfecho.\n"
                "No te derrumbes.\n"
                "No sepas lo que pasa\n"
                "ni lo que ocurre.\n";
        emit textoEjemplo(texto);
    }
    close();
}

void Ejemplo::on_pButtonCancelar_clicked()
{
    close();
}
